
(function ($) {
  $(document).ready(() => {
      $('.slideshow').slick({
          dots: true,
          infinite: true,
          speed: 1000,
          autoplay: true,
          autoplaySpeed: 5000,
          fade: true,
      });
  });
}(jQuery));

(function ($) {
$('.slideshow').on('afterChange', function(event, slick, currentSlide, nextSlide){
  $('.slideshow-image').get(currentSlide).play();
});
}(jQuery));


// modal

let modal = document.querySelector('#myModal');
let close = document.querySelector('.close');
let popupVideo = document.querySelector('#video-popup');

close.addEventListener('click', function(){
    modal.style.display="none";
    popupVideo.pause();
});
  
  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
      popupVideo.pause();      
    }
  };



